# RecyclerViewPager

## Introduction
> RecyclerViewPager is a container that supports custom horizontal and vertical paging effects, with a Material Design style.

![Effect](screenshot/RecyclerViewpager.gif)


## How to Install
```shell
ohpm install @ohos/recyclerviewpager
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).


## How to Use

### Using singleFlingPager
1. Import **singleFlingPager**.
 ```
   import { singleFlingPager } from "@ohos/recyclerviewpager"
 ```

2. Pass in a custom layout.
 ```xml 
 Pass in your custom layout to the custom method specificParam.
        @Builder specificParam(item) {

        if (item.i == this.index) {
        Flex() {
        Text("item=" + item.i).fontSize(this.fontSize).fontColor("#e5e1e1")
        }
        .margin({ left: this.marginLeft, right: this.marginRight })
        .width(this.Containderwidth)
        .height('90%')
        .backgroundColor("#273238")
        .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 + this.offsetX / this.ScreenOffset : 1 - this.offsetX / this.ScreenOffset
        })
        .offset({x:'1%'})
        } else {
        Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
        }
        .margin({ left: this.marginLeft, right: this.marginRight })
        .width(this.Containderwidth)
        .height('80%')
        .backgroundColor("#273238")
        .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 - this.offsetX / this.ScreenOffset : 1 + this.offsetX / this.ScreenOffset
        })
        .offset({x:'1%'})
        }
        }
 ```

3. Pass in the layout to the container.
 ```
    build() {
    Column() {
      Flex({ direction: FlexDirection.Column }) {
        singleFlingPager(
          {
            arr: this.arr,
            offsetX: this.offsetX!!,
            index: this.index!!,
            marginLeft: this.marginLeft,
            marginRight: this.marginRight,
            Containderwidth: this.Containderwidth,
            ContainderHeight: this.ContainderHeight,
            content: (item) => {
              this.specificParam(item)
            }
          }
        )
      }
    }
  }
 ```

### Using verticalViewPager
1. Import **verticalViewPager**.
 ```
   import { verticalViewPager } from "@ohos/recyclerviewpager"
 ```

2. Pass in a custom layout.
 ```xml 
 Pass in your custom layout to the custom method specificParam.
    @Builder specificParam(item) {

    if (item.i == this.index) {
      Flex() {
        Text("item=" + this.index).fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ top: this.topSpaced, bottom: this.topSpaced /*, left: this.topSpaced, right: this.topSpaced */
      })
      .width('100%')
      .height(this.ContainderHeight)
      .backgroundColor("#273238")
      .scale({
        x: this.offsetY < 0 ? 1 + this.offsetY / this.ScreenOffset : 1 - this.offsetY / this.ScreenOffset,
        y: 1
      })
    } else {
      Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ top: this.topSpaced, bottom: this.topSpaced, left: this.topSpaced, right: this.topSpaced })
      .width('90%')
      .height(this.ContainderHeight)
      .backgroundColor("#273238")
      .scale({
        x: this.offsetY < 0 ? 1 - this.offsetY / this.ScreenOffset : 1 + this.offsetY / this.ScreenOffset,
        y: 1
      })
    }

  }
 ```


3. Pass in the layout to the container.
 ```
    build() {
    Column() {
      Flex() {
        verticalViewPager({
          arr: this.arr,
          offsetY: this.offsetY!!,
          index: this.index!!,
          marginTop: this.topSpaced,
          marginBottom: this.topSpaced,
          ContainderWidth: this.ContainderWidth,
          ContainderHeight: this.ContainderHeight,
          content: (item) => {
            this.specificParam(item)
          }
        })
      }
    }
  }
 ```

### Using singleFlingPagerSelect
1. Import singleFlingPagerSelect.
 ```
   import { singleFlingPagerSelect } from "@ohos/recyclerviewpager"
 ```

2. Pass in a custom layout.
 ```
 Pass in your custom layout to the custom method specificParam.
@Builder specificParam(item) {

    if (item.i == this.index) {
      Flex() {
        Text("item=" + item.i).fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft,right: this.marginRight })
      .width(this.Containderwidth)
      .height('90%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 + this.offsetX / this.ScreenOffset : 1 - this.offsetX / this.ScreenOffset
      })
    } else {
      Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft,right: this.marginRight })
      .width(this.Containderwidth)
      .height('80%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 - this.offsetX / this.ScreenOffset : 1 + this.offsetX / this.ScreenOffset
      })
    }

  }
 ```

3. Pass in the layout to the container.
 ```
    build() {
      Flex() {
        singleFlingPagerSelect({
          arr: this.arr,
          offsetX: this.offsetX!!,
          index: this.index!!,
          marginLeft: this.marginLeft,
          marginRight: this.marginRight,
          Containderwidth: this.Containderwidth,
          ContainderHeight: this.ContainderHeight,
          content: (item) => {
            this.specificParam(item)
          }
        })
      )}
    }
 ```

## Attributes

### singleFlingPager Attributes
```xml 
arr: page text content
offsetX: page scrolling offset
index: current page index
marginLeft: left margin of the page
marginRight: right margin of the page
Containderwidth: page width
ContainderHeight: page height
content: container layout
```

### verticalViewPager Attributes
```xml 
arr: page text content
offsetY: page scrolling offset
index: current page index
marginTop: top margin of the page
marginBottomt: bottom margin of the page
ContainderWidth: page width
ContainderHeight: page height
content: container layout
```

### singleFlingPagerSelect Attributes
```xml 
arr: page text content
offsetX: page scrolling offset
index: current page index
marginLeft: left margin of the page
marginRight: right margin of the page
Containderwidth: page width
ContainderHeight: page height
content: container layout
```


## Constraints

This project has been verified in the following versions:
- DevEco Studio: DevEco Studio NEXT Developer Beta3 (5.0.3.521)
- OpenHarmony SDK: API 12 (5.0.0.25)

## Directory Structure
````
|---- RecyclerViewPager 
|     |---- entry  # Sample code
|     |---- library  # RecyclerViewPager library
|         |----src
|             |----main
|                     |----components
|                         |----materialContainderTop.ets # Material style implementation
|                         |----verticalViewPager.ets # Up-down page scroll implementation
|         |---- index.ets  # External APIs
|     |---- README.md  # Readme               
````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/RecyclerViewPager/issues) or a [PR](https://gitee.com/openharmony-sig/RecyclerViewPager/pulls).

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/RecyclerViewPager/blob/master/LICENSE).
